using System;

namespace GaDeRyPoLuKi_GUI
{
    class Pair
    {
        char first_element;
        char second_element;

        public Pair(char element1, char element2)
        {
            first_element = element1;
            second_element = element2;
        }

        public char? getPair(char element)
        {
            if (hasElement(element))
            {
                if (first_element == element)
                {
                    return second_element;
                }
                return first_element;
            }
            return null;
        }

        public bool hasElement(char element)
        {
            if (first_element == element || second_element == element)
            {
                return true;
            }
            return false;
        }

        public void showElements()
        {
            Console.WriteLine($"({first_element}, {second_element})\n");
        }
    }
}
