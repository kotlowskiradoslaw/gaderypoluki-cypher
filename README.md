# Gaderypoluki cypher

A popular among polish scouts groups substitution cipher with a fixed key.

"Gaderypoluki" is a suggested fixed key, but it can be modified.

### How does it work?

Having a fixed key "gaderypoluki" you replace letters in such a manner:
* g <-> a
* d <-> e
* r <-> y
* etc.

### Examples

* World -> Wpyue
* Home -> Hpmd
