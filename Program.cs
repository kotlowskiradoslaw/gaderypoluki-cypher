using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GaDeRyPoLuKi_GUI
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_encoding());
        }
    }
}

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static bool HasUniques(this String str)
        {
            return new HashSet<char>(str).Count() == str.Length;
        }
    }
}
