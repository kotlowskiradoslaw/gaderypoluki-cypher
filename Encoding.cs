using System.Collections.Generic;

namespace GaDeRyPoLuKi_GUI
{
    static class Encoding
    {
        static List<Pair> Code_keys;

        static public string codeText(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                for (int j = 0; j < Code_keys.Count; j++)
                {
                    if (Code_keys[j].hasElement(text[i]))
                    {
                        text = text.Insert(i, Code_keys[j].getPair(text[i]).ToString())
                            .Remove(i + 1, 1);
                        break;
                    }
                }
            }

            return text;
        }

        static public void setCode(string text)
        {
            string textLower = text.Replace(" ", "").ToLower();
            string textUpper = textLower.ToUpper();

            Code_keys = new List<Pair>() { new Pair(textLower[0], textLower[1]) };
            Code_keys.Add(new Pair(textUpper[0], textUpper[1]));

            for (int i = 2; i < textLower.Length; i += 2)
            {
                Code_keys.Add(new Pair(textLower[i], textLower[i + 1]));
                Code_keys.Add(new Pair(textUpper[i], textUpper[i + 1]));
            }
        }

        static public void resetCode()
        {
            Code_keys = new List<Pair>();
        }
    }
}
