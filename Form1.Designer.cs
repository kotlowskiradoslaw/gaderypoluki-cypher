﻿namespace GaDeRyPoLuKi_GUI
{
    partial class Form_encoding
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_encodeText = new System.Windows.Forms.TextBox();
            this.label_encodeText = new System.Windows.Forms.Label();
            this.label_encodedText = new System.Windows.Forms.Label();
            this.textBox_encodedText = new System.Windows.Forms.TextBox();
            this.button_copyResultToClipboard = new System.Windows.Forms.Button();
            this.button_encode = new System.Windows.Forms.Button();
            this.button_loadFile = new System.Windows.Forms.Button();
            this.button_saveToFile = new System.Windows.Forms.Button();
            this.button_cancelFileLoading = new System.Windows.Forms.Button();
            this.button_editFileText = new System.Windows.Forms.Button();
            this.comboBox_encoding = new System.Windows.Forms.ComboBox();
            this.groupBox_elements = new System.Windows.Forms.GroupBox();
            this.label_codeKey = new System.Windows.Forms.Label();
            this.groupBox_elements.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_encodeText
            // 
            this.textBox_encodeText.Location = new System.Drawing.Point(6, 55);
            this.textBox_encodeText.Multiline = true;
            this.textBox_encodeText.Name = "textBox_encodeText";
            this.textBox_encodeText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_encodeText.Size = new System.Drawing.Size(776, 120);
            this.textBox_encodeText.TabIndex = 2;
            this.textBox_encodeText.TextChanged += new System.EventHandler(this.textBox_encodeText_TextChanged);
            // 
            // label_encodeText
            // 
            this.label_encodeText.AutoSize = true;
            this.label_encodeText.Location = new System.Drawing.Point(6, 27);
            this.label_encodeText.Name = "label_encodeText";
            this.label_encodeText.Size = new System.Drawing.Size(245, 25);
            this.label_encodeText.TabIndex = 1;
            this.label_encodeText.Text = "Wpisz tekst do zaszyfrowania";
            // 
            // label_encodedText
            // 
            this.label_encodedText.AutoSize = true;
            this.label_encodedText.Location = new System.Drawing.Point(6, 221);
            this.label_encodedText.Name = "label_encodedText";
            this.label_encodedText.Size = new System.Drawing.Size(201, 25);
            this.label_encodedText.TabIndex = 4;
            this.label_encodedText.Text = "Oto zaszyfrowany tekst:";
            // 
            // textBox_encodedText
            // 
            this.textBox_encodedText.Location = new System.Drawing.Point(6, 249);
            this.textBox_encodedText.Multiline = true;
            this.textBox_encodedText.Name = "textBox_encodedText";
            this.textBox_encodedText.ReadOnly = true;
            this.textBox_encodedText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_encodedText.Size = new System.Drawing.Size(776, 120);
            this.textBox_encodedText.TabIndex = 3;
            this.textBox_encodedText.TabStop = false;
            // 
            // button_copyResultToClipboard
            // 
            this.button_copyResultToClipboard.Location = new System.Drawing.Point(667, 375);
            this.button_copyResultToClipboard.Name = "button_copyResultToClipboard";
            this.button_copyResultToClipboard.Size = new System.Drawing.Size(115, 37);
            this.button_copyResultToClipboard.TabIndex = 7;
            this.button_copyResultToClipboard.Text = "Kopiuj";
            this.button_copyResultToClipboard.UseVisualStyleBackColor = true;
            this.button_copyResultToClipboard.Click += new System.EventHandler(this.button_copyResultToClipboard_Click);
            // 
            // button_encode
            // 
            this.button_encode.Location = new System.Drawing.Point(679, 38);
            this.button_encode.Name = "button_encode";
            this.button_encode.Size = new System.Drawing.Size(115, 37);
            this.button_encode.TabIndex = 1;
            this.button_encode.Text = "Szyfruj";
            this.button_encode.UseVisualStyleBackColor = true;
            this.button_encode.Click += new System.EventHandler(this.button_encode_Click);
            // 
            // button_loadFile
            // 
            this.button_loadFile.Location = new System.Drawing.Point(6, 181);
            this.button_loadFile.Name = "button_loadFile";
            this.button_loadFile.Size = new System.Drawing.Size(145, 37);
            this.button_loadFile.TabIndex = 3;
            this.button_loadFile.Text = "Załaduj z pliku";
            this.button_loadFile.UseVisualStyleBackColor = true;
            this.button_loadFile.Click += new System.EventHandler(this.button_loadFile_Click);
            // 
            // button_saveToFile
            // 
            this.button_saveToFile.Location = new System.Drawing.Point(6, 375);
            this.button_saveToFile.Name = "button_saveToFile";
            this.button_saveToFile.Size = new System.Drawing.Size(145, 37);
            this.button_saveToFile.TabIndex = 6;
            this.button_saveToFile.Text = "Zapisz do pliku";
            this.button_saveToFile.UseVisualStyleBackColor = true;
            this.button_saveToFile.Click += new System.EventHandler(this.button_saveToFile_Click);
            // 
            // button_cancelFileLoading
            // 
            this.button_cancelFileLoading.Location = new System.Drawing.Point(308, 181);
            this.button_cancelFileLoading.Name = "button_cancelFileLoading";
            this.button_cancelFileLoading.Size = new System.Drawing.Size(145, 37);
            this.button_cancelFileLoading.TabIndex = 5;
            this.button_cancelFileLoading.Text = "Anuluj";
            this.button_cancelFileLoading.UseVisualStyleBackColor = true;
            this.button_cancelFileLoading.Visible = false;
            this.button_cancelFileLoading.Click += new System.EventHandler(this.button_cancelFileLoading_Click);
            // 
            // button_editFileText
            // 
            this.button_editFileText.Location = new System.Drawing.Point(157, 181);
            this.button_editFileText.Name = "button_editFileText";
            this.button_editFileText.Size = new System.Drawing.Size(145, 37);
            this.button_editFileText.TabIndex = 4;
            this.button_editFileText.Text = "Edytuj";
            this.button_editFileText.UseVisualStyleBackColor = true;
            this.button_editFileText.Visible = false;
            this.button_editFileText.Click += new System.EventHandler(this.button_editFileText_Click);
            // 
            // comboBox_encoding
            // 
            this.comboBox_encoding.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.comboBox_encoding.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_encoding.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboBox_encoding.FormattingEnabled = true;
            this.comboBox_encoding.ItemHeight = 28;
            this.comboBox_encoding.Items.AddRange(new object[] {
            "Gaderypoluki",
            "Mięsny robaczek",
            "Politykarenu"});
            this.comboBox_encoding.Location = new System.Drawing.Point(18, 38);
            this.comboBox_encoding.Name = "comboBox_encoding";
            this.comboBox_encoding.Size = new System.Drawing.Size(655, 36);
            this.comboBox_encoding.TabIndex = 0;
            this.comboBox_encoding.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox_encoding_KeyDown);
            // 
            // groupBox_elements
            // 
            this.groupBox_elements.AutoSize = true;
            this.groupBox_elements.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_elements.Controls.Add(this.textBox_encodeText);
            this.groupBox_elements.Controls.Add(this.label_encodeText);
            this.groupBox_elements.Controls.Add(this.button_editFileText);
            this.groupBox_elements.Controls.Add(this.textBox_encodedText);
            this.groupBox_elements.Controls.Add(this.button_cancelFileLoading);
            this.groupBox_elements.Controls.Add(this.label_encodedText);
            this.groupBox_elements.Controls.Add(this.button_saveToFile);
            this.groupBox_elements.Controls.Add(this.button_copyResultToClipboard);
            this.groupBox_elements.Controls.Add(this.button_loadFile);
            this.groupBox_elements.Enabled = false;
            this.groupBox_elements.Location = new System.Drawing.Point(12, 81);
            this.groupBox_elements.Name = "groupBox_elements";
            this.groupBox_elements.Size = new System.Drawing.Size(788, 442);
            this.groupBox_elements.TabIndex = 8;
            this.groupBox_elements.TabStop = false;
            // 
            // label_codeKey
            // 
            this.label_codeKey.AutoSize = true;
            this.label_codeKey.Location = new System.Drawing.Point(18, 10);
            this.label_codeKey.Name = "label_codeKey";
            this.label_codeKey.Size = new System.Drawing.Size(52, 25);
            this.label_codeKey.TabIndex = 9;
            this.label_codeKey.Text = "Klucz";
            // 
            // Form_encoding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 530);
            this.Controls.Add(this.label_codeKey);
            this.Controls.Add(this.groupBox_elements);
            this.Controls.Add(this.comboBox_encoding);
            this.Controls.Add(this.button_encode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_encoding";
            this.Text = "Szyfrowanie";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox_elements.ResumeLayout(false);
            this.groupBox_elements.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_encodeText;
        private System.Windows.Forms.Label label_encodeText;
        private System.Windows.Forms.Label label_encodedText;
        private System.Windows.Forms.TextBox textBox_encodedText;
        private System.Windows.Forms.Button button_copyResultToClipboard;
        private System.Windows.Forms.Button button_encode;
        private System.Windows.Forms.Button button_loadFile;
        private System.Windows.Forms.Button button_saveToFile;
        private System.Windows.Forms.Button button_cancelFileLoading;
        private System.Windows.Forms.Button button_editFileText;
        private System.Windows.Forms.ComboBox comboBox_encoding;
        private System.Windows.Forms.GroupBox groupBox_elements;
        private System.Windows.Forms.Label label_codeKey;
    }
}

