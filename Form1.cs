﻿using System;
using System.IO;
using System.Windows.Forms;
using ExtensionMethods;

namespace GaDeRyPoLuKi_GUI
{
    public partial class Form_encoding : Form
    {
        public Form_encoding()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_encode_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(comboBox_encoding.Text))
            {
                MessageBox.Show("Nie wybrano ani nie wprowadzono szyfrowania.");
            }
            else if (!comboBox_encoding.Text.HasUniques())
            {
                MessageBox.Show("W szyfrowaniu znaki nie mogą się powtarzać.");
            }
            else if (comboBox_encoding.Text.Replace(" ", "").Length % 2 == 1)
            {
                MessageBox.Show("Klucz szyfrowania musi mieć parzystą liczbę znaków.");
            }
            else
            {
                if (groupBox_elements.Enabled)
                {
                    Encoding.resetCode();
                    comboBox_encoding.Enabled = true;
                    button_encode.Text = "Szyfruj";
                    groupBox_elements.Enabled = false;
                }
                else
                {
                    Encoding.setCode(comboBox_encoding.Text);
                    textBox_encodeText_TextChanged(this, new EventArgs());
                    comboBox_encoding.Items.Add(comboBox_encoding.Text);

                    comboBox_encoding.Enabled = false;
                    button_encode.Text = "Zmień";
                    groupBox_elements.Enabled = true;
                }
            }
        }

        private void button_copyResultToClipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox_encodedText.Text);
        }

        private void button_loadFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectedFile = new OpenFileDialog();
            selectedFile.Filter = "Pliki tekstowe (*.txt)|*.txt";
            if (selectedFile.ShowDialog() == DialogResult.OK)
            {
                textBox_encodeText.Text = File.ReadAllText(selectedFile.FileName);
                textBox_encodeText.ReadOnly = true;
                button_cancelFileLoading.Visible = true;
                button_editFileText.Visible = true;
            }
        }

        private void button_cancelFileLoading_Click(object sender, EventArgs e)
        {
            textBox_encodeText.ReadOnly = false;
            button_cancelFileLoading.Visible = false;
            button_editFileText.Visible = false;
            textBox_encodeText.ResetText();
            textBox_encodedText.ResetText();
        }

        private void button_saveToFile_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog saveFile = new FolderBrowserDialog();
            DialogResult result = saveFile.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(saveFile.SelectedPath))
            {
                if (!string.IsNullOrWhiteSpace(textBox_encodedText.Text))
                {
                    DateTime now = DateTime.Now;
                    string dir = (saveFile.SelectedPath + $"\\szyfrowanie_{now}.txt".Replace(':', '-')).Replace(' ', '_');
                    string fileHeader = $"Szyfrowanie tekstu z {now}\n\n";
                    File.WriteAllTextAsync(dir, fileHeader + textBox_encodedText.Text);
                    MessageBox.Show($"Pomyślnie zapisano do pliku {dir}.");
                }
                else
                {
                    MessageBox.Show("Brak tekstu do zapisu.");
                }
            }
        }

        private void textBox_encodeText_TextChanged(object sender, EventArgs e)
        {
            textBox_encodedText.Text = Encoding.codeText(textBox_encodeText.Text);
        }

        private void button_editFileText_Click(object sender, EventArgs e)
        {
            textBox_encodeText.ReadOnly = !textBox_encodeText.ReadOnly;
            button_editFileText.Text = textBox_encodeText.ReadOnly ? "Edytuj" : "Wstrzymaj";
        }

        private void comboBox_encoding_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;

                button_encode_Click(this, new EventArgs());
                this.ProcessTabKey(true);
            }
        }
    }
}
